import socket
import json
from flask_cors import CORS, cross_origin
from flask import Flask, Response, request, jsonify, render_template
from json import dumps
import threading
from flask.views import  MethodView
import struct

class DataBase():
    missionState = 'stop'

    def addServer(self, server):
        self.server = server

    def changeMissionState(self, state):
        self.missionState = state
        self.server.serverSocket.sendMessage(self.missionState)

db = DataBase()

class serverSocket():
    PORT, HOST_IP = 5700, '127.0.0.1'
    key = 4

    def run(self):
        print('start python/c++ server socket')
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((self.HOST_IP, self.PORT))
            s.listen()
            print("starting to listen")
            self.conn, addr = s.accept()
            with self.conn:
                print('Connected by', addr)
                self.sendMessage('stop')
                while True:
                    i = 0


    def sendMessage(self, message):
        t = 0
        if message == "start":
            t = 1
        else:
            t = 0
        d = struct.pack('I', t)
        self.conn.sendall(d)

class webServerSocket():

    serverHttp = Flask(__name__)

    def __init__(self):
        self.cors = CORS(self.serverHttp, resources={r"/api/*": {"origins": "*"}})
        self.generateView()

    @serverHttp.after_request
    def after_request(response):
        response.headers.add('Access-Control-Allow-Origin', '*')
        response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
        response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
        return response

    def generateView(self):
        self.serverHttp.add_url_rule('/user/', view_func=User.as_view('user'))
        self.serverHttp.add_url_rule('/state/', view_func=State.as_view('state'))

    def run(self):
        print('start web server socket')
        self.serverHttp.run(port=4800)

class Server:
    def __init__(self):
        self.serverSocket = serverSocket()
        self.webServerSocket = webServerSocket()

    def run(self):
        t1 = threading.Thread(target=self.serverSocket.run)
        t2 = threading.Thread(target=self.webServerSocket.run)
        t1.start()
        t2.start()

class User(MethodView):

    def __init__(self):
        self.db = db

    def get(self):
        return 'get user'

    def post(self):
        print('POST REQUEST')
        self.username = request.get_json()['name']
        print(self.username)
        return Response(dumps({
            'content': 'Bonjour ' + self.username + ', tu es bien connecté au serveur ! félicitation !! Nous pouvons commencer ...!'
        }), mimetype='text/json')

class State(MethodView):

    def __init__(self):
        self.db = db

    def get(self):
        return 'get state'

    def post(self):
        print('POST REQUEST')
        self.db.changeMissionState(request.get_json()['state'])
        return Response(dumps({
            'content': 'Bonjour ' + self.db.missionState + ', tu es bien connecté au serveur ! félicitation !! Nous pouvons commencer ...!'
        }), mimetype='text/json')



if __name__ == "__main__":
    server = Server()
    db.addServer(server)
    server.run()
