/* Include the controller definition */
#include "crazyflie_sensing.h"
#include "socket.h"

/* Function definitions for XML parsing */
#include <argos3/core/utility/configuration/argos_configuration.h>
/* 2D vector definition */
#include <argos3/core/utility/math/rng.h>
#include <argos3/core/utility/math/vector2.h>
/* Logging */
#include <argos3/core/utility/logging/argos_log.h>
#include <argos3/core/utility/math/vector3.h>

/****************************************/
/****************************************/

CCrazyflieSensing::CCrazyflieSensing() :
   m_pcDistance(NULL),
   m_pcPropellers(NULL),
   m_pcRNG(NULL),
   m_pcRABA(NULL),
   m_pcRABS(NULL),
   m_pcPos(NULL),
   m_pcBattery(NULL),
   m_uiCurrentStep(0) {
   sockfd = serversock::createConnection();
}

/****************************************/
/****************************************/

void CCrazyflieSensing::Init(TConfigurationNode& t_node) {
   try {
      /*
       * Initialize sensors/actuators
       */
      m_pcDistance   = GetSensor  <CCI_CrazyflieDistanceScannerSensor>("crazyflie_distance_scanner");
      m_pcPropellers = GetActuator  <CCI_QuadRotorPositionActuator>("quadrotor_position");
      /* Get pointers to devices */
      m_pcRABA   = GetActuator<CCI_RangeAndBearingActuator>("range_and_bearing");
      m_pcRABS   = GetSensor  <CCI_RangeAndBearingSensor  >("range_and_bearing");
      try {
         m_pcPos = GetSensor  <CCI_PositioningSensor>("positioning");
      }
      catch(CARGoSException& ex) {}
      try {
         m_pcBattery = GetSensor<CCI_BatterySensor>("battery");
      }
      catch(CARGoSException& ex) {}      
   }
   catch(CARGoSException& ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error initializing the crazyflie sensing controller for robot \"" << GetId() << "\"", ex);
   }
   /*
    * Initialize other stuff
    */
   /* Create a random number generator. We use the 'argos' category so
      that creation, reset, seeding and cleanup are managed by ARGoS. */
   m_pcRNG = CRandom::CreateRNG("argos");

   m_uiCurrentStep = 0;
   Reset();
}

/****************************************/
/****************************************/

void CCrazyflieSensing::ControlStep() {
   // Dummy behavior: takeoff for 10 steps, 
   // then moves in a square shape for 200 steps then lands.

   struct serversock::objectData data;

   // Takeoff
   int ret = serversock::readValues(&data, sockfd);
   command = ret == -1 ? command : ret;
   if (command == 1) {
     if (m_cInitialPosition.GetZ() != 0.5f) {
       m_cInitialPosition = m_pcPos->GetReading().Position;
       m_cInitialPosition.SetZ(0.5f);
     }
     TakeOff();
   } else if (command == 0) {
     Land();
   }
}

/****************************************/
/****************************************/

void CCrazyflieSensing::TakeOff() {
  CVector3 cPos = m_pcPos->GetReading().Position;
  if (Abs(cPos.GetZ() - 0.5f) < 0.01f) {
    Explore();
  } else {
    cPos.SetZ(0.5f);
    m_pcPropellers->SetAbsolutePosition(cPos);
  }
}

/****************************************/
/****************************************/

void CCrazyflieSensing::Explore() {
  int minDistance = 80;
  // Square pattern
  CCI_CrazyflieDistanceScannerSensor::TReadingsMap sDistRead =
      m_pcDistance->GetReadingsMap();
  auto iterDistRead = sDistRead.begin();
  double distances[4] = {(iterDistRead++)->second, (iterDistRead++)->second,
                         (iterDistRead++)->second, (iterDistRead)->second};

  CVector3 trans;
  bool tooCloseToFront = (distances[0] < minDistance) && (distances[0] != -2);
  bool tooCloseToLeft = (distances[1] < minDistance) && (distances[1] != -2);
  bool tooCloseToBack = (distances[2] < minDistance) && (distances[2] != -2);
  bool tooCloseToRight = (distances[3] < minDistance) && (distances[3] != -2);
  bool tooClose =
      tooCloseToFront || tooCloseToLeft || tooCloseToBack || tooCloseToRight;

  if (tooCloseToFront) {
    MoveBackwards(0.25f);
  }
  if (tooCloseToLeft) {
    MoveRight(0.25f);
  }
  if (tooCloseToBack) {
    MoveForward(0.25f);
  }
  if (tooCloseToRight) {
    MoveLeft(0.25f);
  }

  if(!tooClose) {
      if(m_pcRNG->Bernoulli()) {
          if(m_pcRNG->Bernoulli())
              MoveForward(0.1f);
          else
              MoveLeft(0.1f);
      } else {
          if(m_pcRNG->Bernoulli())
              MoveRight(0.1f);
          else
              MoveBackwards(0.1f);
      }
  }

}

bool CCrazyflieSensing::Land() {
   CVector3 cPos = m_pcPos->GetReading().Position;
   if(Abs(cPos.GetZ()) < 0.01f) return false;
   cPos.SetZ(0.0f);
   m_pcPropellers->SetAbsolutePosition(cPos);
   return true;
}

/****************************************/
/****************************************/

void CCrazyflieSensing::Reset() {
}

/****************************************/
/****************************************/

void CCrazyflieSensing::MoveForward(float distance) {
   CVector3 currentPosition = m_pcPos->GetReading().Position;
   m_pcPropellers->SetAbsolutePosition(currentPosition + CVector3(0.0f, -distance, 0.0f));
}

void CCrazyflieSensing::MoveLeft(float distance) {
   CVector3 currentPosition = m_pcPos->GetReading().Position;
   m_pcPropellers->SetAbsolutePosition(currentPosition + CVector3(distance, 0.0f, 0.0f));
}

void CCrazyflieSensing::MoveBackwards(float distance) {
   CVector3 currentPosition = m_pcPos->GetReading().Position;
   m_pcPropellers->SetAbsolutePosition(currentPosition + CVector3(0.0f, distance, 0.0f));
}

void CCrazyflieSensing::MoveRight(float distance) {
   CVector3 currentPosition = m_pcPos->GetReading().Position;
   m_pcPropellers->SetAbsolutePosition(currentPosition + CVector3(-distance, 0.0f, 0.0f));
}

/*
 * This statement notifies ARGoS of the existence of the controller.
 * It binds the class passed as first argument to the string passed as
 * second argument.
 * The string is then usable in the XML configuration file to refer to
 * this controller.
 * When ARGoS reads that string in the XML file, it knows which controller
 * class to instantiate.
 * See also the XML configuration files for an example of how this is used.
 */
REGISTER_CONTROLLER(CCrazyflieSensing, "crazyflie_sensing_controller")
