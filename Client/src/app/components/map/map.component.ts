import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MapService } from 'src/app/services/map/map.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  @ViewChild('canvas', { static: false }) 
  canvas: ElementRef<HTMLCanvasElement>;

  previewCtx: CanvasRenderingContext2D;
  
  constructor(private mapService:MapService) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.mapService.ctx = this.canvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
    this.mapService.drawMap()
  }

}
