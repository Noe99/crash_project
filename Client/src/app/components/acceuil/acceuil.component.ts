import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { HttpRequestService } from '../../services/http/http-request.service'

@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.scss']
})
export class AcceuilComponent implements OnInit {

  text = 'Server: Not connected for the moment ...'
  name = ''
  items: any;
  user: User = {
    name: 'Noe'
  };

  constructor(private httpRequestService: HttpRequestService) {
  }

  ngOnInit(): void {
  }

  testConnection(): void {

    this.httpRequestService.postTestConnection(this.user).subscribe(
      data => {
        this.text = data.content
        console.log(data.content)
      }
    )

    // this.httpRequestService.getTestConnection().subscribe(
    //   data =>{
    //     this.items = data.content
    //     console.log(this.items)
    //   }
    // )
  }
}
