import { Component, ContentChildren, Injectable, OnInit } from '@angular/core';
import { MissionState } from 'src/app/interfaces/missionState';
import { HttpRequestService } from 'src/app/services/http/http-request.service';
import { MissionService } from 'src/app/services/mission/mission.service';
import { MapComponent } from '../map/map.component';
@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})
export class ApplicationComponent implements OnInit {


  constructor(private httpRequestService: HttpRequestService, private missionService: MissionService) { }

  ngOnInit(): void {
  }

  startMission():void{
    console.log('Mission start')
    this.missionService.state = 'start'
    this.httpRequestService.postMissionState(this.createMissionState(this.missionService.state)).subscribe(
      data => {
        console.log(data.content)
      }
    )
  }

  stopMission():void{
    console.log('Mission stop')
    this.missionService.state = 'stop'
    this.httpRequestService.postMissionState(this.createMissionState(this.missionService.state)).subscribe(
      data => {
        console.log(data.content)
      }
    )
  }

  createMissionState(state):MissionState{
    let missionState: MissionState = { state: state}
    return missionState
  }
}
