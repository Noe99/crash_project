import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { User } from 'src/app/interfaces/user';
import { MissionState } from 'src/app/interfaces/missionState';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {
  url = "http://localhost:4800/"

  constructor(private http: HttpClient) { }

  public getTestConnection(): Observable<any> {
    console.log('send request GET');
    return this.http.get(this.url + 'get/');
  }

  public postTestConnection(user: User): Observable<any> {
    console.log('send request POST');
    return this.http.post(this.url + 'user/', user);
  }

  public postMissionState(state: MissionState): Observable<any> {
    console.log('send request POST');
    return this.http.post(this.url + 'state/', state);
  }
}
