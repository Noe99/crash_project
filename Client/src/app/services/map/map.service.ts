import { ElementRef, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  map=[
    1,1,1,1,1,1,
    1,0,0,0,0,1,
    1,0,1,1,0,1,
    1,0,0,0,0,1,
    1,1,1,1,1,1,
  ]

  public ctx: CanvasRenderingContext2D;

  constructor() { }

  public drawMap():void{  
    this.ctx.fillStyle = 'red';
    this.ctx.fillRect(200, 200, 25, 25);
    console.log('draw rect')
  }
}
